module.exports = function(connection) {

    var User = require('./user')(connection);    
    var Transaction = require('./transaction')(connection);
    var PushHistory = require('./pushHistory')(connection);
    var Message = require('./message')(connection);

    return {
        user: User,
        message: Message,
        transaction: Transaction,
        pushHistory: PushHistory
    }
}