var mongoose = require('mongoose');

module.exports = function(connection) {

    var Schema = mongoose.Schema;

    var pushHistorySchema = new Schema({
        userName: String,
        targetName: String,
        content: String,
        time: String
    });

    var PushHistory = connection.model('PushHistory', pushHistorySchema);

    return PushHistory;
}