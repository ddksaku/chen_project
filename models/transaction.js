var mongoose = require('mongoose');

module.exports = function(connection) {

    var Schema = mongoose.Schema;

    var transactionSchema = new Schema({
        user_id: String,
        text: String,
        time: String,
        coin: Number
    });

    var Transaction = connection.model('Transaction', transactionSchema);

    return Transaction;
}