var mongoose = require('mongoose');
var moment = require('moment');

module.exports = function(connection) {

    var Schema = mongoose.Schema;

    var userSchema = new Schema({
        userName: {type: String, required: true, default: ""},
        password: {type: String, required: true, default: ""},
        email: {
            address: String,
            token: String,
            verified: {type: Boolean, required: true, default: false}
        },
        phoneNumber: {
            number: String,
            verified: {type: Boolean, required: true, default: false}
        },
        token : {
            createDate: {type: Date, required: true, default: moment()},
            lastChangedDate: {type: Date, required: true, default: moment()}
        },
        status: {type: String, required: true, default: "off"},
        description: {type: String, default: ""},
        gender: {type: String, required: true, default: "m"},
        visible: {type: Boolean, required: true, default: true},
        avatar_url: {type: String, default: ""},
        background_url: {type: String, default: ""},  
        coin: {type: Number, required: true, default: 0},
        costReply: {type: Number, default: 0},
        pushToken: {type: String, default: ""},
        follower: [{
            type: mongoose.Schema.Types.ObjectId
        }],
        following: [{
            type: mongoose.Schema.Types.ObjectId
        }],
        isAdmin: {type: Boolean, required: true, default: false}
    });

    var User = connection.model('User', userSchema);

    return User;
}