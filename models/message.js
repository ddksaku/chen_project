var mongoose = require('mongoose');

module.exports = function(connection) {

    var Schema = mongoose.Schema;

    var messageSchema = new Schema({
        user_id: String,
        senderName: String,
        senderPhoto: String,
        receiver: String,
        receiverName: String,
        receiverPhoto: String,
        sender_id: String,
        cost: {
            view: Number,
            replay: Number,
            replayTip: Number,
        },
        content: {
            text: String,
            url: String
        },
        textHeight: Number,    
        create: Date,
        isPhoto: Boolean,
        isViewed: Boolean,
        isReplied: Boolean,        
        isReply: Boolean
    });

    var Message = connection.model('Message', messageSchema);

    return Message;
}