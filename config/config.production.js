var config = require('./config.global');

config.env = 'production';

config.push_server = "gateway.push.apple.com";
config.push_product = true;

module.exports = config;