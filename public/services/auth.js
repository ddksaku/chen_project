angular.module('FamestarApp')

.factory('Auth', ['$http', '$location', '$rootScope', '$alert', '$route', 'config',
    function($http, $location, $rootScope, $alert, $route, config) {
      $rootScope.isAuthenticated = false;
      var alertMsg = function(title, text, place, type, time) {
        $alert({
          title: title,
          content: text,
          placement: place,
          type: type,
          duration: time
        });
      };

      var removeToken = function() {
        config.token = "";
        $rootScope.isAuthenticated = false;
      };

      var getAllUser = function() {
        $http.get(config.baseUrl + '/api/getAllUser?token=' + config.token)
        .success(function(data) {
          if(data.success) {
            config.users = data.info;
            $location.path('/main');
          } else {
            removeToken();
            $location.path('/login');
            alertMsg('Error!', data.message, 'top-right', 'danger', 3);
          }
        })
        .error(function(response) {
          alertMsg('Error!', response.data, 'top-right', 'danger', 3);
        });
      };

      var getUser = function(id, isChange) {
        $http.get(config.baseUrl + '/api/getUserById?token=' + config.token + '&user_id=' + id)
        .success(function(data) {
          if(data.success) {
            config.user = data.info;
            if(isChange) {
              $route.reload();
              alertMsg('Cheers!', 'Success!', 'top-right', 'success', 3);
            } else {
              $location.path('/user');
            }            
          } else {
            removeToken();
            $location.path('/login');
            alertMsg('Error!', data.message, 'top-right', 'danger', 3);
          }
        })
        .error(function(response) {
          alertMsg('Error!', response.data, 'top-right', 'danger', 3);
        });
      };

      var getTransaction = function(id) {
        $http.get(config.baseUrl + '/api/getTransaction?token=' + config.token + '&user_id=' + id)
        .success(function(data) {
          if(data.success) {
            config.transaction = data.info;
            $location.path('/transaction');
          } else {
            removeToken();
            $location.path('/login');
            alertMsg('Error!', data.message, 'top-right', 'danger', 3);
          }
        })
        .error(function(response) {
          alertMsg('Error!', response.data, 'top-right', 'danger', 3);
        });
      };

      var getMessage = function(id) {
        $http.get(config.baseUrl + '/api/getMessage?token=' + config.token + '&user_id=' + id)
        .success(function(data) {
          if(data.success) {
            config.message.in = data.in;
            config.message.out = data.out;
            $location.path('/message');
          } else {
            removeToken();
            $location.path('/login');
            alertMsg('Error!', data.message, 'top-right', 'danger', 3);
          }
        })
        .error(function(response) {
          alertMsg('Error!', response.data, 'top-right', 'danger', 3);
        });
      };

      var getPushHis = function(name) {
        $http.get(config.baseUrl + '/api/getPushHis?token=' + config.token + '&userName=' + name)
        .success(function(data) {
          if(data.success) {
            config.pushHis = data.info;
            $location.path('/pushHis');
          } else {
            removeToken();
            $location.path('/login');
            alertMsg('Error!', data.message, 'top-right', 'danger', 3);
          }
        })
        .error(function(response) {
          alertMsg('Error!', response.data, 'top-right', 'danger', 3);
        });
      };

      var chageUser = function(user) {
        $http.post(config.baseUrl + '/api/setAdminChangeUser', user)
        .success(function(data) {
          if(data.success) {            
            getUser(config.user._id, true);
          } else {
            alertMsg('Error!', "Failed. Try again!", 'top-right', 'danger', 3);
          }
        })
        .error(function(response) {
          alertMsg('Error!', "response.data", 'top-right', 'danger', 3);
        });
      }

      return {
        login: function(user) {
          return $http.post(config.baseUrl + '/api/admin/login', user)
            .success(function(data) {
              if(data.success) {
                config.token = data.token;
                config.id = data.info;
                $rootScope.isAuthenticated = true;
                getAllUser();
                alertMsg('Cheers!', 'You have successfully logged in.', 'top-right', 'success', 3);
              } else {
                config.token = "";
                $rootScope.isAuthenticated = false;
                alertMsg('Error!', 'Invalid username or password.', 'top-right', 'danger', 3);
              }
            })
            .error(function(response) {
              alertMsg('Error!', response.data, 'top-right', 'danger', 3);
            });
        },
        signup: function(user) {
          return $http.post(config.baseUrl + '/api/admin/reset', user)
            .success(function(data) {
              removeToken();
              if(data.success) {                
                $location.path('/login');
                alertMsg('Congratulations!', 'Your account has been changed.', 'top-right', 'success', 3);
              } else {
                alertMsg('Error!', 'Invalid old password.', 'top-right', 'danger', 3);
              }
            })
            .error(function(response) {
              alertMsg('Error!', response.data, 'top-right', 'danger', 3);
            });
        },
        logout: function() {          
          removeToken();
          $location.path('/login');
          alertMsg('Ok!', 'You have been logged out.', 'top-right', 'info', 3);         
        },
        getAllUser: getAllUser,
        getUser: getUser,
        getTransaction: getTransaction,
        getMessage: getMessage,
        getPushHis: getPushHis,
        chageUser: chageUser
      }
    }
]);