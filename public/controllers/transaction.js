angular.module('FamestarApp')

.controller('TransactionCtrl', ['$scope', 'config', function($scope, config) {
    config.state = 2;
    $scope.transaction = config.transaction;
    if(config.user.avatar_url == "") {
    	$scope.img_url = "images/user.png";
    } else {
    	$scope.img_url = config.user.avatar_url;
    }
    $scope.userName = config.user.userName;
}]);