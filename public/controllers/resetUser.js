angular.module('FamestarApp')

.controller('ResetCtrl', ['$scope', 'Auth', function($scope, Auth) {
    $scope.signup = function() {
      	Auth.signup({
        	userName: $scope.name,
        	password: $scope.password,
        	oldPassword: $scope.oldPassword
      	});
    };
}]);