angular.module('FamestarApp')

.controller('PushHisCtrl', ['$scope', 'config', function($scope, config) {
    config.state = 2;
    $scope.pushHis = config.pushHis;
    if(config.user.avatar_url == "") {
    	$scope.img_url = "images/user.png";
    } else {
    	$scope.img_url = config.user.avatar_url;
    }
    $scope.userName = config.user.userName;
}]);