angular.module('FamestarApp')

.controller('MessageCtrl', ['$scope', '$sce', 'config', function($scope, $sce, config) {
    config.state = 2;
    $scope.message = config.message;
    if(config.user.avatar_url == "") {
    	$scope.img_url = "images/user.png";
    } else {
    	$scope.img_url = config.user.avatar_url;
    }
    $scope.userName = config.user.userName;

    $scope.inUrl = [];
    for(var i = 0; i < $scope.message.in.length; i++) {    	
    	$scope.inUrl[i] = $sce.trustAsResourceUrl($scope.message.in[i].content.url);
    }
    $scope.outUrl = [];
    for(var i = 0; i < $scope.message.out.length; i++) {    	
    	$scope.outUrl[i] = $sce.trustAsResourceUrl($scope.message.out[i].content.url);
    }
}]);