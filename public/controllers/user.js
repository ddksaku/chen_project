angular.module('FamestarApp')

.controller('UserCtrl', ['$scope', 'Auth', 'config', function($scope, Auth, config) {
    config.state = 1;
    $scope.user = config.user;
    if($scope.user.avatar_url == "") {
    	$scope.user.avatar_url = "images/user.png";
    }
    if($scope.user.background_url == "") {
    	$scope.user.background_url = "images/back.png";
    }
    if($scope.user.gender == "m") {
        $scope.user.gender = "male";
    } else {
        $scope.user.gender = "female";
    }
    $scope.name = $scope.user.userName;
    $scope.coin = $scope.user.coin;
    
    $scope.getTransaction = function(id) {
    	Auth.getTransaction(id);
    }

    $scope.getMessage = function(id) {
    	Auth.getMessage(id);
    }

    $scope.getPushHis = function(name) {
    	Auth.getPushHis(name);
    }

    $scope.change = function() {
        if($scope.name != $scope.user.userName || $scope.coin != $scope.user.coin) {
            Auth.chageUser({
                targetName: $scope.name,
                targetCoin: $scope.coin,
                target_id: $scope.user._id,
                user_id: config.id,
                token: config.token
            });
        }
    };
}]);