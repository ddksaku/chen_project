angular.module('FamestarApp')

.controller('MainCtrl', ['$scope', 'Auth', 'config', function($scope, Auth, config) {
    config.state = 0;
    $scope.users = config.users;
    for(var i = 0; i < $scope.users.length; i++) {
    	if($scope.users[i].avatar_url == "") {
	    	$scope.users[i].avatar_url = "images/user.png";
	    }
	    if($scope.users[i].gender == "m") {
	    	$scope.users[i].gender = "male";
	    } else {
	    	$scope.users[i].gender = "female";
	    }
    }   

    $scope.getUser = function(id) {
    	Auth.getUser(id, false);
    }
}]);