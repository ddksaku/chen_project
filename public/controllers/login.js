angular.module('FamestarApp')

.controller('LoginCtrl', ['$scope', 'Auth', function($scope, Auth) {
    $scope.login = function() {
     	Auth.login({
        	userName: $scope.name,
        	password: $scope.password
      	});
    };
}]);