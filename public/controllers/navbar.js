angular.module('FamestarApp')

.controller('NavbarCtrl', ['$scope', '$location', 'Auth', 'config', function($scope, $location, Auth, config) {
    $scope.logout = function() {
      	Auth.logout();
    };

    $scope.back = function() {
    	if(config.state == 1) {
    		Auth.getAllUser();
    	} else if(config.state == 2) {
    		Auth.getUser(config.user._id);
    	}
    }
}]);