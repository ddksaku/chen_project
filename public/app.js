angular.module('FamestarApp', ['ngCookies', 'ngResource', 'ngMessages', 'ngRoute', 'mgcrea.ngStrap', 'ngLoadingSpinner'])

.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider
    .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
    })
    .when('/resetUser', {
        templateUrl: 'views/resetUser.html',
        controller: 'ResetCtrl'
    })
    .when('/main', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
    })
    .when('/user', {
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
    })
    .when('/transaction', {
        templateUrl: 'views/transaction.html',
        controller: 'TransactionCtrl'
    })
    .when('/message', {
        templateUrl: 'views/message.html',
        controller: 'MessageCtrl'
    })
    .when('/pushHis', {
        templateUrl: 'views/pushHis.html',
        controller: 'PushHisCtrl'
    })
    .otherwise({
        redirectTo: '/login'
    });
}])

.value('config', {
    baseUrl: "http://localhost:8081",
    //baseUrl: "http://52.36.57.133:8081",
    token: "",
    id: "",
    state: 0,
    users: {},
    user: {},
    message: {},
    transaction: {},
    pushHis: {}
});
