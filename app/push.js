var apn = require('apn');
var cfg = require('../config');

module.exports = function() {
    var options = {
        gateway : cfg.push_server,
        cert: __dirname + '/keys/cert.pem',
        key: __dirname + '/keys/key.pem',
        production: cfg.push_product
    };

    var apnConnection = new apn.Connection(options);
    apnConnection.on("connected", function() {
        console.log("push connected");
    });

    apnConnection.on("transmitted", function(notification, device) {
        console.log("Notification transmitted to:" + device.token.toString("hex"));
    });

    apnConnection.on("transmissionError", function(errCode, notification, device) {
        console.error("Notification caused error: " + errCode + " for device ");
    });

    apnConnection.on("timeout", function () {
        console.log("push connection Timeout");
    });

    apnConnection.on("disconnected", function() {
        console.log("push disconnected from APNS");
    });

    apnConnection.on("socketError", console.error);

    return {

        sendPushFollower: function(sender_name, receiver_token, type) {            
            var note = new apn.Notification();
            note.badge = 1;
            if(type == 0) {
                note.alert = "New follower is added.";
            } else {
                note.alert = "Unfollower.";
            }
            apnConnection.pushNotification(note, receiver_token);
        },

        sendPushMessage: function(sender_name, receiver_token, type) {
            var note = new apn.Notification();
            note.badge = 1;
            if(type == 'message') {
                note.alert = sender_name + " has just sent you message.";
            } else if(type == 'reply') {
                note.alert = sender_name + " has replied to your message.";
            }
            
            apnConnection.pushNotification(note, receiver_token);
        },

        sendPushCoin: function(coin, receiver_token) {
            var note = new apn.Notification();
            note.badge = 1;            
            note.alert = 'costvalue' + coin;
            apnConnection.pushNotification(note, receiver_token);       
        }
    }
}