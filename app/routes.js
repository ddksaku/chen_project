var cfg = require('../config');
var jwt = require('jsonwebtoken');

module.exports = function(app, models) {
    var api = require('./api.js')(models);

    //admin
    app.post('/api/admin/login', api.adminLogin);
    app.post('/api/admin/reset', api.adminReset);

    //user manage
    app.post('/api/signup', api.signup);
    app.post('/api/login', api.login);
    app.get('/api/verify', api.verify);
    app.get('/verified', function(req, res){
        res.render('verified', {'id': req.params.id});
    });
    app.post('/api/resetPassword', api.resetPassword);
    app.get('/api/forgotPassword', api.forgotPassword);
    
    //authentication
    app.use(function(req, res, next) {
       console.log("sfsfsfsfs"); 
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        if (token) {
            jwt.verify(token, cfg.secret, function(err, decoded) {      
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });    
                } else {                    
                    next();
                }
            });
        } else {
            return res.json({ success: false, message: 'No token provided.' });
        }
    });

    app.post('/api/logout', api.logout);
    app.post('/api/setPhoneNumber', api.setPhoneNumber);
    app.post('/api/checkVerify', api.checkVerify);    
    app.post('/api/setPushToken', api.setPushToken);

    app.post('/api/profileUpload', api.profileUpload);
    app.post('/api/backUpload', api.backUpload);
    app.post('/api/setCostReply', api.setCostReply);

    app.post('/api/setDescription', api.setDescription);
    app.post('/api/setVisible', api.setVisible);
    app.get('/api/getUserById', api.getUserById);
    app.get('/api/getAllUser', api.getAllUser);
    app.post('/api/deleteUser', api.deleteUser);
    
    //relationship
    app.post('/api/setFollow', api.setFollow);
    app.post('/api/setUnFollow', api.setUnFollow);
    app.get('/api/getTransaction', api.getTransaction);
    app.get('/api/getFollower', api.getFollower);
    app.get('/api/getFollowing', api.getFollowing);
    app.get('/api/getPushHis', api.getPushHis);
    
    //message
    app.post('/api/sendMessage', api.sendMessage);
    app.post('/api/sendReply', api.sendReply);
    app.post('/api/viewMessage', api.viewMessage);
    app.get('/api/getMessage', api.getMessage);
    app.get('/api/getInMessage', api.getInMessage);
    app.get('/api/getOutMessage', api.getOutMessage);
    app.get('/api/getReplyMessage', api.getReplyMessage);
    app.post('/api/addCoin', api.addCoin);
    app.post('/api/deleteMessage', api.deleteMessage);
    app.post('/api/setAdmin', api.setAdmin);
    app.post('/api/setAdminChangeUser', api.setAdminChangeUser);
}