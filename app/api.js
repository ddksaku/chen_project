var moment = require('moment');
var uuid = require('node-uuid');
var nodemailer = require('nodemailer');
var jwt = require('jsonwebtoken');

var cfg = require('../config');
var client = require('twilio')(cfg.twilio_account_sid, cfg.twilio_auth_token);

module.exports = function(models) {
    var dataPro = require('./dataPro.js')(models);
    var push = require('./push.js')();
    var User = models.user;
    var Transaction = models.transaction;
    var Message = models.message;
    var PushHistory = models.pushHistory;

    return {
       
        signup: function (req, res)
        {
            var body = req.body;
            User.find({$or: [{'email.address': body.email}, {userName: body.userName}]}, function(err, users) {
                if (users[0]) {
                    res.json({'success': false, 'message': 'User already exist!'});
                } else {
                    var email = {
                        address: body.email,
                        token: ''
                    }

                    var newUser = new User({
                        email: email, 
                        password: dataPro.toHash(body.password),
                        gender: body.gender,
                        userName: body.userName,
                        status: "on",
                        follower: [],
                        following: []
                    });

                    dataPro.saveUser(res, newUser, function(user) {
                        var token = jwt.sign(user._id, cfg.secret, {
                            expiresIn: '24h' // expires in 24 hours
                        });
                        res.json({'success': true, 'info': user, 'token': token});
                    });
                }
            });
        },

        login: function (req, res)
        {
            var body = req.body;
            dataPro.findUser(res, body, "email", function(user) {
                if (user) {              
                    if(dataPro.toHash(body.password) == user.password) {
                        user.status = "on";
                        dataPro.saveUser(res, user, function(user) {
                            var token = jwt.sign(user._id, cfg.secret, {
                                expiresIn: '24h' // expires in 24 hours
                            });

                            Message.find({'receiver': user._id, 'isReply': true, 'isViewed': false}, function(err, messages) {
                                if(err) {
                                    res.json({'success': false, 'message': err});
                                } else {
                                    var replayMsgLength = dataPro.removeMessage(messages).length;                   
                                    res.json({'success': true, 'info': user, 'token': token, 'replayMsgLength': replayMsgLength});
                                }
                            });                            
                        });  
                    } else {
                        res.json({'success': false, 'message': 'Password is invalid!'});
                    }                                        
                } else {                    
                    res.json({'success': false, 'message': 'Email is invalid!'});
                }
            });
        },

        setPushToken: function (req, res) {
            var body = req.body;    
            dataPro.findUser(res, body, "user_id", function(user) {
                if (user) {
                    user.pushToken = body.pushToken;
                    dataPro.saveUser(res, user, function(user) {
                        res.json({'success': true, 'info': user});
                    }); 
                } else {
                    res.json({'success': false, 'message': 'User not found!'});
                }
            });
        },

        logout: function (req, res)
        {
            var body = req.body;
            dataPro.findUser(res, body, "user_id", function(user) {
                if (user) {            
                    user.status = "off";
                    dataPro.saveUser(res, user, function(user) {
                        res.json({'success': true});
                    });
                }
            });
        },

        setPhoneNumber: function (req, res)
        {
            var body = req.body;
            dataPro.findUser(res, body, "user_id", function(user) {
                if (user) {            
                    user.phoneNumber = {
                        number: body.phoneNumber,
                        verified: false
                    };
                    dataPro.saveUser(res, user, function(user) {
                        var code = Math.floor((Math.random()*999999)+111111);
                        client.sendSms({
                            to: user.phoneNumber.number,
                            from: cfg.twilio_phone_number,
                            body: 'Your verification code is: ' + code
                        }, function(twilioerr, responseData) {
                            if (twilioerr) {                      
                                res.json({'success': false, 'message': 'Invalid phone number!'});
                            } else {
                                res.json({'success': true, 'code': code});
                            }
                        });                        
                    });
                }
            });
        },

        checkVerify: function (req, res)
        {
            var body = req.body;
            dataPro.findUser(res, body, "user_id", function(user) {
                if (user) {
                    user.phoneNumber.verified = body.verified;
                    dataPro.saveUser(res, user, function(user) {
                        res.json({'success': true, 'info': user});
                    });
                }
            });
        },

        forgotPassword: function (req, res)
        {
            var query = req.query;
            dataPro.findUser(res, query, "email", function(user) {
                if (user) {
                    var token = uuid.v4();

                    var transport = nodemailer.createTransport({
                        host: cfg.smtp_host,
                        secure: true,
                        auth: {
                            user: cfg.smtp_user,
                            pass: cfg.smtp_pass
                        }
                    });

                    var mailOptions = {
                        from: cfg.smtp_sender,
                        to: user.email.address,
                        subject: 'Forgot Password!',
                        html:'<h1>Please click below link.</h1><a href="' + req.protocol + '://' + req.get('host') + '/api/verify?token=' + token + '">reset password.</a>'
                    };

                    transport.sendMail(mailOptions, function(error, response){
                        transport.close(); 
                        if (error){          
                            res.json({'success': false, 'message': 'Email is invalid!'});
                        } else {
                            user.email.token = token;
                            user.email.verified = false;
                            dataPro.saveUser(res, user, function(user) {
                                res.json({'success': true, 'message': 'Sent mail to your email address!'});
                            });
                        }
                    });
                } else {                    
                    res.json({'success': false, 'message': 'Email is invalid!'});
                }
            });
        },

        verify: function (req, res)
        {
            var query = req.query;
            dataPro.findUser(res, query, "token", function(user) {
                if (user) {
                    user.email.token = '';
                    user.email.verified = true;
                    dataPro.saveUser(res, user, function(user) {
                        res.redirect('/verified?id=' + user._id);
                    });
                } else {                    
                    res.json({'success': false, 'message': 'Email is invalid!'});
                }
            });
        },

        resetPassword: function (req, res) {            
            var body = req.body;
            dataPro.findUser(res, body, "user_id", function(user) {
                if (user) {
                    user.password = dataPro.toHash(body.password)
                    dataPro.saveUser(res, user, function(user) {
                        res.json({'success': true});
                    });
                } else {                    
                    res.json({'success': false});
                }
            });
        },

        profileUpload: function (req, res) 
        {                
            var body = req.body;
            dataPro.findUser(res, body, "user_id", function(user) {
                if (user) {
                    user.avatar_url = body.avatar;
                    dataPro.saveUser(res, user, function(user) {
                        res.json({'success': true, 'info': user});
                    });
                }
            });
        },

        backUpload: function (req, res) 
        {
            var body = req.body;
            dataPro.findUser(res, body, "user_id", function(user) {
                if (user) {
                    user.background_url = body.back;
                    dataPro.saveUser(res, user, function(user) {
                        res.json({'success': true, 'info': user});
                    });
                }
            });
        },

        setCostReply: function(req, res)
        {
            var body = req.body;
            dataPro.findUser(res, body, "user_id", function(user) {
                if (user) {                    
                    user.costReply = body.cost;
                    dataPro.saveUser(res, user, function(user) {
                        Message.find({'user_id': body.user_id, 'isReplied': false}, function(err, messages) {
                            if(err) {
                                res.json({'success': false, 'message': err});
                            } else {
                                for(var i = 0; i < messages.length; i++) {
                                    messages[i].cost.replay = body.cost;
                                    messages[i].save();
                                }
                                res.json({'success': true, 'info': user});
                            }
                        })                        
                    });
                } else {                    
                    res.json({'success': false});
                }
            });
        },

        getTransaction: function(req, res)
        {
            var query = req.query;
            Transaction.find({user_id: query.user_id}, function(err, transactions) {
                if (err) {
                    res.json({'success': false, 'message': err});
                } else {
                    res.json({'success': true, 'info': transactions});
                }
            });
        },

        getFollower: function (req, res) {
            var query = req.query;
            dataPro.findUser(res, query, "user_id", function(user) {
                User.find({_id: {$in: user.follower}}, function(err, users) {
                    res.json({'success': true, 'info': users});
                });
            });
        },

        getFollowing: function (req, res) {
            var query = req.query;
            dataPro.findUser(res, query, "user_id", function(user) {
                User.find({_id: {$in: user.following}}, function(err, users) {
                    res.json({'success': true, 'info': users});
                });
            });
        },

        setFollow: function (req, res) {
            var body = req.body;
            dataPro.findUser(res, body, "user_id", function(user) {
                var index = user.following.indexOf(body.target_id);
                if(index == -1) {
                    user.following.push(body.target_id);
                }                
                dataPro.saveUser(res, user, function(userSave) {
                    User.findOne({_id: body.target_id}, function(err, target) {
                        var index = target.follower.indexOf(body.user_id);
                        if(index == -1) {
                            target.follower.push(body.user_id);
                        }
                        
                        dataPro.saveUser(res, target, function(targetSave) {
                            push.sendPushFollower(userSave.userName, targetSave.pushToken, 0);

                            var pushHis = new PushHistory({
                                userName: userSave.userName,
                                targetName: targetSave.userName,
                                content: "New follower is added.",
                                time: moment().format('DD-MM-YYYY HH:mm')
                            });
                            pushHis.save();

                            res.json({'success': true, 'info': userSave});
                        });
                    });
                });
            });
        },

        setUnFollow: function (req, res) {
            var body = req.body;
            dataPro.findUser(res, body, "user_id", function(user) {
                var index = user.following.indexOf(body.target_id);
                if(index > -1) {
                    user.following.splice(index, 1);
                }
                dataPro.saveUser(res, user, function(userSave) {
                    User.findOne({_id: body.target_id}, function(err, target) {
                        var index = target.follower.indexOf(body.user_id);
                        if(index > -1) {
                            target.follower.splice(index, 1);
                        }
                        dataPro.saveUser(res, target, function(targetSave) {
                            push.sendPushFollower(userSave.userName, targetSave.pushToken, 1);

                            var pushHis = new PushHistory({
                                userName: userSave.userName,
                                targetName: targetSave.userName,
                                content: "Unfollower.",
                                time: moment().format('DD-MM-YYYY HH:mm')
                            });
                            pushHis.save();

                            res.json({'success': true, 'info': userSave});
                        });
                    });
                });
            });
        },

        setDescription: function (req, res) {
            var body = req.body;
            dataPro.findUser(res, body, "user_id", function(user) {
                if (user) {
                    user.description = body.description;
                    dataPro.saveUser(res, user, function(user) {
                        res.json({'success': true, 'info': user});
                    });
                } else {                    
                    res.json({'success': false});
                }
            });
        },

        setVisible: function (req, res) {
            var body = req.body;
            dataPro.findUser(res, body, "user_id", function(user) {
                if (user) {
                    user.visible = body.visible;
                    dataPro.saveUser(res, user, function(user) {
                        res.json({'success': true, 'info': user});
                    });
                } else {                    
                    res.json({'success': false});
                }
            });
        },

        getUserById: function (req, res) {
            User.findById(req.query.user_id, function (err, user) {
                if (err) {
                    res.json({'success': false, 'message': err});
                } else {
                    res.json({'success': true, 'info': user});
                }
            });
        },

        getAllUser: function (req, res) {
            User.find(function (err, users) {
                if (err) {
                    res.json({'success': false, 'message': err});
                } else {
                    res.json({'success': true, 'info': users});
                }
            });
        },

        deleteUser: function (req, res) {
            User.remove({_id: req.body.user_id}, function (err) {
                if (err) {
                    res.json({'success': false, 'message': err});
                } else {                   
                    res.json({'success': true});                      
                }
            });
        },

        sendMessage: function (req, res) {
            var body = req.body;
            dataPro.findUser(res, body, "user_id", function(user) {
                if (user) {
                    var cost = {
                        view: parseInt(body.view),
                        replay: user.costReply,
                        replayTip: 0
                    }
                    var content = {
                        text: body.text,
                        url: body.content
                    }           

                    var targetArr = body.receivers.split(",");                   
                    for(var i = 0; i < targetArr.length; i++) {                                         
                        User.findOne({_id: targetArr[i]}, function(err, target) {
                            if (target) {                                                    
                                push.sendPushMessage(user.userName, target.pushToken, 'message');
                                var newMsg = new Message({
                                    user_id: user._id,
                                    senderName: user.userName,
                                    senderPhoto: user.avatar_url,
                                    receiver: target._id,
                                    receiverName: target.userName,
                                    receiverPhoto: target.avatar_url,
                                    sender_id: body.sender_id,
                                    cost: cost,
                                    content: content,
                                    textHeight: parseFloat(body.textHeight),
                                    isViewed: false,
                                    isReplied: false,
                                    isPhoto: body.isPhoto,
                                    create: new Date(Date.now()),
                                    isReply: false
                                });
                                newMsg.save();

                                var pushHis = new PushHistory({
                                    userName: user.userName,
                                    targetName: target.userName,
                                    content: "Send message.",
                                    time: moment().format('DD-MM-YYYY HH:mm')
                                });
                                pushHis.save();
                            }
                        });
                    }
                    res.json({'success': true});
                } else {
                    res.json({'success': false});
                }                                      
            });            
        },

        sendReply: function (req, res) {
            var body = req.body;
            dataPro.findUser(res, body, "user_id", function(user) {
                if (user) {              
                    User.findOne({_id: body.target_id}, function(err, target) {
                        if (target) {
                            var content = {
                                text: body.text,
                                url: body.content
                            }

                            var cost = {
                                replay: 0,
                                view: 0,
                                replayTip: 0
                            }

                            if(body.isSender == "1") {
                                cost.view = parseInt(body.view);
                                cost.replay = user.costReply;
                            } else {
                                cost.replayTip = parseInt(body.replyTip);
                                user.coin = user.coin - parseInt(body.replyTip);
                                user.save(function(err, userN) {
                                    push.sendPushCoin(userN.coin, userN.pushToken);
                                    var newTran = new Transaction({
                                        user_id: userN._id,
                                        text: "You replyed " + target.userName + "'s message for " + body.replyTip + " coins.",                                    
                                        time: moment().format('DD-MM-YYYY HH:mm'),
                                        coin: userN.coin
                                    });    
                                    newTran.save();

                                    target.coin = target.coin + parseInt(body.replyTip);
                                    target.save();     
                                    newTran = new Transaction({
                                        user_id: target._id,
                                        text: userN.userName + " replyed your message for " + body.replyTip + " coins.",
                                        time: moment().format('DD-MM-YYYY HH:mm'),
                                        coin: target.coin
                                    });
                                    newTran.save();
                                    push.sendPushCoin(target.coin, target.pushToken);
                                });                                
                            }

                            push.sendPushMessage(user.userName, target.pushToken, 'reply');
                            var newMsg = new Message({
                                user_id: user._id,
                                senderName: user.userName,
                                senderPhoto: user.avatar_url,
                                receiver: target._id,
                                receiverName: target.userName,
                                receiverPhoto: target.avatar_url,
                                sender_id: body.sender_id,
                                cost: cost,
                                content: content,
                                textHeight: parseFloat(body.textHeight),
                                isViewed: false,
                                isReplied: false,
                                isPhoto: body.isPhoto,
                                create: new Date(Date.now()),
                                isReply: true
                            });
                            newMsg.save();

                            var pushHis = new PushHistory({
                                userName: user.userName,
                                targetName: target.userName,
                                content: "Send message.",
                                time: moment().format('DD-MM-YYYY HH:mm')
                            });
                            pushHis.save();

                            Message.findOne({_id: body.message_id}, function(err, message) {
                                if(message) {
                                    message.isReplied = true;
                                    message.save();
                                    res.json({'success': true, "info": user});
                                } else {
                                    res.json({'success': false});
                                }
                            });                            
                        } else {
                            res.json({'success': false});
                        }  
                    });                    
                } else {
                    res.json({'success': false});
                }                                         
            });            
        },

        viewMessage: function(req, res) {
            var body = req.body;
            Message.findOne({_id: body.message_id}, function(err, message) {
                if(message) {
                    message.isViewed = true;
                    message.save(function(err, message) {
                        if (err){
                            res.json({'success': false, 'message': err});
                        } else {
                            User.findOne({_id: message.user_id}, function(err, user) {
                                if (user) {
                                    User.findOne({_id: message.receiver}, function(err, me) {
                                        if(message.cost.view != 0) {                                     
                                            user.coin = user.coin + message.cost.view;
                                            user.save();
                                            var newTran = new Transaction({
                                                user_id: user._id,
                                                text: me.userName + " viewed your message for " + message.cost.view + " coins.",
                                                time: moment().format('DD-MM-YYYY HH:mm'),
                                                coin: user.coin
                                            });
                                            newTran.save();
                                            push.sendPushCoin(user.coin, user.pushToken);

                                            me.coin = me.coin - message.cost.view;
                                            me.save(function(err, userN) {
                                                push.sendPushCoin(userN.coin, userN.pushToken);
                                                newTran = new Transaction({
                                                    user_id: body.user_id,
                                                    text: "You viewed a message from " + user.userName + " for " + message.cost.view + " coins.",
                                                    time: moment().format('DD-MM-YYYY HH:mm'),
                                                    coin: userN.coin
                                                });
                                                newTran.save();

                                                res.json({'success': true, 'info': me});
                                            });
                                        } else {
                                            res.json({'success': true, 'info': me});
                                        }                                         
                                    });
                                } else {
                                    res.json({'success': false, 'message': err});
                                }
                            });                      
                        }
                    })
                }
            });
        },

        getReplyMessage : function(req, res) {
            Message.find({'receiver': req.query.user_id, 'isReply': true}, function(err, messages) {
                if(err) {
                    res.json({'success': false, 'message': err});
                } else {
                    var msg = dataPro.removeMessage(messages);                   
                    res.json({'success': true, 'info': msg});
                }
            })
        },

        getInMessage: function(req, res) {
            Message.find({'receiver': req.query.user_id, 'isReply': false}, function(err, messages_in) {
                if(err) {
                    res.json({'success': false, 'message': err});
                } else {
                    var inMsg = dataPro.removeMessage(messages_in);                   
                    res.json({'success': true, 'info': inMsg});
                }
            })
        },

        getOutMessage: function(req, res) {           
            Message.find({'user_id': req.query.user_id, 'isReply': false}, function(err, messages_out) {
                if(err) {
                    res.json({'success': false, 'message': err});
                } else {
                    var outMsg = dataPro.removeMessage(messages_out);
                    res.json({'success': true, 'info': outMsg});
                }
            })
        },

        getMessage: function(req, res) {
            Message.find({receiver: req.query.user_id}, function(err, messages_in) {
                if(err) {
                    res.json({'success': false, 'message': err});                    
                } else {
                    
                    Message.find({user_id: req.query.user_id}, function(err, messages_out) {
                        if(err) {
                            res.json({'success': false, 'message': err});
                        } else {
                            var inMsg = dataPro.removeMessage(messages_in);
                            var outMsg = dataPro.removeMessage(messages_out);
                            res.json({'success': true, 'in': inMsg, 'out': outMsg});
                        }
                    })
                }
            })
        },

        addCoin: function(req, res) {
            var body = req.body;
            User.findOne({_id: body.user_id}, function(err, user) {
                if(user) {
                    user.coin = user.coin + parseInt(body.coin);
                    dataPro.saveUser(res, user, function(user) {
                        var newTran = new Transaction({
                            user_id: body.user_id,
                            text: "You purchased " + body.coin + " coins for " + body.money + "$.",
                            time: moment().format('DD-MM-YYYY HH:mm'),
                            coin: user.coin
                        });
                        newTran.save(function(err, transaction) {
                            if (err){
                                res.json({'success': false, 'message': err});
                            } else {
                                res.json({'success': true, 'info': user});
                            }
                        });
                    });
                } else {
                    res.json({'success': false, 'message': err});
                }
            })
        },

        getPushHis: function(req, res) {
            PushHistory.find({userName: req.query.userName}, function(err, pushHis) {
                if(err) {
                    res.json({'success': false, 'message': err});
                } else {
                    res.json({'success': true, 'info': pushHis});
                }
            })
        },

        deleteMessage: function (req, res) {
            Message.remove({_id: req.body.message_id}, function (err) {
                if (err) {
                    res.json({'success': false, 'message': err});
                } else {                   
                    res.json({'success': true});                      
                }
            });
        },

        adminLogin: function (req, res) {
            var body = req.body;
            User.findOne({userName: body.userName}, function(err, admin) {
                if(admin && admin.isAdmin) {         
                    if(admin.password == dataPro.toHash(body.password)) {
                        var token = jwt.sign(admin._id, cfg.secret, {
                            expiresIn: '24h' // expires in 24 hours
                        });
                        res.json({'success': true, 'token': token, 'info': admin._id});
                    } else {
                        res.json({'success': false});
                    }
                } else {
                    res.json({'success': false});
                }
            });
        },

        adminReset: function (req, res) {
            var body = req.body;
            User.findOne({password: dataPro.toHash(body.oldPassword)}, function(err, admin) {
                if(admin && admin.isAdmin) {
                    admin.userName = body.userName;
                    admin.password = dataPro.toHash(body.password);
                    admin.save(function(err, admin) {
                        if(err) {
                            res.json({'success': false});
                        } else {
                            res.json({'success': true});
                        }
                    });
                } else {
                    res.json({'success': false});
                }
            });
        },

        setAdminChangeUser: function (req, res) {
            var body = req.body;
            User.findOne({_id: body.user_id}, function(err, admin) {
                if(admin && admin.isAdmin) {
                    User.findOne({_id: body.target_id}, function(err, target) {
                        if(err) {
                            res.json({'success': false});
                        } else {
                            target.userName = body.targetName;
                            target.coin = parseInt(body.targetCoin);
                            target.save(function(err, user) {
                                if(err) {
                                    res.json({'success': false});
                                } else {
                                    var newTran = new Transaction({
                                        user_id: user._id,
                                        text: "You was changed name or coin.",
                                        time: moment().format('DD-MM-YYYY HH:mm'),
                                        coin: user.coin
                                    });     
                                    newTran.save(function(err, transaction) {
                                        if (err){
                                            res.json({'success': false});
                                        } else {
                                            res.json({'success': true});
                                        }
                                    });
                                }
                            });
                        }
                    });                    
                } else {
                    res.json({'success': false});
                }
            });
        },

        setAdmin: function (req, res) {
            User.findOne({_id: req.body.user_id}, function(err, admin) {
                if(err) {
                    res.json({'success': false});
                } else {
                    admin.isAdmin = true;
                    admin.save();
                    res.json({'success': true});
                }
            })
        }
    }
}