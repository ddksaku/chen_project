var moment = require('moment');
var crypto = require('crypto');

module.exports = function(models) {

    var User = models.user;
    var Message = models.message;

    return {

        findUser: function (res, body, index_key, next)
        {   
            var searchText = {
                "email": {'email.address': body.email},
                "user_id": {_id: body.user_id},
                "token": {'email.token': body.token}
            }; 
            User.findOne(searchText[index_key], function(err, user) {
                if (err) {
                    res.json({'success': false, 'message': err});
                } else {
                    return next(user);
                }
            });
        },

        saveUser: function(res, user, next) {
            user.token.lastChangedDate = moment();
            user.save(function (err, user) {
                if (err){
                    res.json({'success': false, 'message': err});
                } else {
                    return next(user);
                }
            });
        },

        toHash: function(pass) {
             var shasum = crypto.createHash('sha1');                
            shasum.update(pass);
            return shasum.digest('hex');
        },

        removeMessage: function(messages) {
            /*var nowTime = new Date(Date.now());
            var result = [];
            for(var i = 0; i < messages.length; i++) {
                if(nowTime - messages[i].create > 86400000) {
                    messages[i].remove();                    
                } else {
                    result.push(messages[i]);
                }
            }*/
            return messages;
        }
    }
}