var express  = require('express');
var http = require('http');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser  = require('body-parser');
var mongoose = require('mongoose');
var connection = require('./config/database')(mongoose);
var models = require('./models/models')(connection);
 
var app = express();
app.set('port', 8081);
app.use(cookieParser());
app.use(logger('dev'));
app.set('view engine', 'html');
app.engine('.html', require('ejs').__express);
app.set('views', __dirname + '/public/views');
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

require('./app/routes.js')(app, models);

var server = http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + JSON.stringify(server.address()));
});